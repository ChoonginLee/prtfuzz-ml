import glob
import re
import dpkt

fw = open("ftp_output.txt", 'w')

for file in glob.glob("./ftp_pcap/*/*.pcap"):

	print
	print "====== ", file, "======"
	print
	f = open(file, 'rb')

	pp = dpkt.pcap.Reader(f)

	for ts, buf in pp:
		try:
			eth = dpkt.ethernet.Ethernet(buf)
			if not isinstance(eth.data, dpkt.ip.IP):
				continue
			ip = eth.data
			if isinstance(ip.data, dpkt.tcp.TCP):
				tcp = ip.data
				if tcp.dport == 21:
						msg = tcp.data
						sport = tcp.sport
						if len(msg) > 0:
							split_msg = re.split("\r\n| |", msg)
							#print split_msg
							msg_fin = ""
							for i in range(len(split_msg)):
								if i != len(split_msg)-1:
									msg_fin += split_msg[i]
									msg_fin += " "
							fw.write(msg_fin)
							fw.write("\n")

		except dpkt.dpkt.NeedData:
			continue

	f.close()

fw.close()